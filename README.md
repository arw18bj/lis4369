# LIS4369 Extensible Enterprise Solutions

## Avery Withers

### LIS4369 Requirements:

*Course Work Links:*

1. [A1_README.md](a1/a1_README.md "My a1 README.md file")
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code
    - Create "a1_tip_calculator" applications
    - Create "a1 tip calculator" Jupyter Notebook
    - Provide screenshots of installation
    - Create Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions
2. [A2_README.md](a2/a2_README.md "My a2 README.md file")
    - Backward-engineer the screenshots provided of "payroll"
    - Backward-engineer screenshots of skillsets 1-3
    - Provide screenshots of "payroll" with and without overtime
    - Provide screenshots of skillsets 1-3
3. [A3_README.md](a3/a3_README.md "My a3 README.md file")
    - Backward-engineer the screenshots provided of "paint estimator"
    - Backward-engineer screenshots of skillsets 4-6
    - Provide screenshots of "paint estimator" running in VSC and JN
    - Provide a3_paint_estimator.ipynb file
    - Provide screenshots of skillsets 4-6
4. [A4_README.md](a4/a4_README.md "My a4 README.md file")
    - Run demo.py
    - If errors, more than likely missing installations.
    - Test Python Package Installer: pip freeze
    - Research how to install any missing packages:
    - Create at least three functions that are called by the program:
        - main(): calls at least two other functions.
        - get_requirements(): displays the program requirements.
        - data_analysis_2(): displays results as per demo.py.
    - Display graph as per instructions w/in demo.py.
5. [A5_README.md](a5/a5_README.md "My a5 README.md file")
    - Do skillsets 13-15
    - Complete the learn_to_use_r tutorial
    - Code and run demo parts 1 and 2
6. [Project_README.md](p/p_README.md "My project README.md file")
    - Run demo.py
    - If errors, more than likely missing installations
    - Test Python Package Installer: pip freeze
    - Research how to do the following installations:
        - pandas (only if missing)
        - pandas-datareader (only if missing)
        - matplotlib (only if missing)
    - Create at least three fumctions that are called by the program:
        - main(): calls at least two other functions.
        - get_requirements(): displays the program requirements.
        - data_analysis_1(): displays the following data.