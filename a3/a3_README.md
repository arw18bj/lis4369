# LIS4369 Extensible Enterprise Solutions

## Avery Withers

### Assignment 3 Requirements:

1. Calculate home interior paint cost (w/o primer).
2. Must use float data types.
3. Must use SQFT_PER_GALLON constant (350).
4. Must use iteration structure (aka "loop").
5. Format, right-align numbers, and round to two decimal places.
6. Create at least five functions that are called by the program:
    * main(): calls two other functions: get_requirements() and estimate_painting_cost()
    * get_requirements(): displays the program requirements.
    * estimate_painting_cost(): calculates interior home painting, and calls print functions.
    * print_painting_estimate(): displays painting costs.
    * print_painting_percentage(): displays painting costs percentages.

#### README.md file should include the following items:

* Assingment requirements
* Screenshot of a3_paint_estimator running in Visual Studio Code
* Screenshot of a3_paint_estimator running in Jupyter Notebook and include .ipynb file [A3 Paint Estimator .ipynb file](code/paint_estimator.ipynb "Paint Estimator")
* Screenshots of a3 skillsets (4-6)

#### Assignment Screenshots:

*Screenshot of a3_paint_estimator application running in Visual Studio Code*:

![A3 Paint Estimator VSC](img/a3_vsc.png)

*Screenshot of a3_paint_estimator application running in Jupyter Notebook*:

![A3 Paint Estimator JN](img/a3_jn.png)

*Screenshot of a3 skillsets (4-6)*:

ss4                              |ss5                              |ss6
:-----------------------:|:-----------------------:|:-----------------------:
![](img/calorie_percentage.png)|![](img/selection_structures.png)|![](img/loops.png)