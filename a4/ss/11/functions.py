import random

def get_requirements():
    print("Pseudo-Random Number Generator")
    print("\nDeveloper: Avery Withers")
    print("\nProgram Requirements:\n"
    + "1. Get user beginning and ending integer values, and store in two variables.\n"
    + "2. Display 10 pseudo-random numbers between, and including, above values.\n"
    + "3. Must use integer data types.\n"
    + "4. Example 1: Using range() and randint() functions.\n"
    + "5. Example 2: Using a list with range() and shuffle() functions.\n")

def random_numbers():
    start = 0
    end = 0

    print("Input:")
    start = int(input("Enter beginning value: "))
    end = int(input("Enter ending value: "))

    """ print() Parameters:
    Generic syntax: print(*objects, sep='', end='\n', file=sys.stdout, flush=False)
    objects: object(s) to be printed. * indicates may be more than one object. Multiple comma-seperated objects or sinble object.
    sep: objects seperated by sep. Default value: '' (space)
    end: printed at last. Default: new line character (\n)
    file: must be object with write(string) method. If omitted, sys.stdout used as default, prints to screen.
    flush - If True, stream is forcibly flushed. Default value: False
    """

    my_sequence = range(6)
    for item in my_sequence:
        print(item)

    print("\nOutput:")
    print("Example 1: Using range() and randint() functions:")

    for item in range(10):
        print(random.randint(start, end), sep=", ", end=" ")

    print()

    print("\nExample 2: Using a list, with range() and shuffle() functions:")

    my_list = list(range(start, end + 1))
    random.shuffle(my_list)
    for item in my_list:
        print(item, sep=", ", end=" ")

    print()