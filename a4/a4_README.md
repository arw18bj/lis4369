# LIS4369 Extensible Enterprise Solutions

## Avery Withers

### Assignment 4 Requirements:

1. Run demo.py
2. If errors, more than likely missing installations.
3. Test Python Package Installer: pip freeze
4. Research how to install any missing packages:
5. Create at least three functions that are called by the program:
    a. main(): calls at least two other functions.
    b. get_requirements(): displays the program requirements.
    c. data_analysis_2(): displays results as per demo.py.
6. Display graph as per instructions w/in demo.py.

#### README.md file should include the following items:

* Assingment requirements, as per A1.
* Screenshot as per example below, including graph.
* Upload A4 .ipynb file and create link in README.md
    * [A4 Demo.ipynb file](demo.ipynb "Demo")

#### Assignment Screenshots:

*Screenshot of demo running in vscode*:

![Demo VSC](img/demo.png)

*Screenshot of demo plot*:

![Demo Plot](img/demo_plot.png)

*Screenshot of demo.ipynb 1*:

![First demo.ipynb](img/demo.ipynb_1.png)

*Screenshot of demo.ipynb 2*:

![Second demo.ipynb](img/demo.ipynb_2.png)

*Screenshot of demo.ipynb 3*:

![Third demo.ipynb](img/demo.ipynb_3.png)

*Screenshot of a3 skillsets (10-12)*:

ss10                              |ss11                              |ss12
:-----------------------:|:-----------------------:|:-----------------------:
![](img/ss10.png)|![](img/ss11.png)|![](img/ss12.png)