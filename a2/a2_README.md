# LIS4369 Extensible Enterprise Solutions

## Avery Withers

### Assignment 2 Requirements:

1. Must use float data type for user input
2. Overtime rate: 1.5 times hourly rate (hours over 40)
3. Holiday rate: 2.0 times hourly rate (all holiday hours)
4. Must format currency with dollar sign, and round to two decimal places
5. Create at least three functions that are called by the program:
    * main(): calls at least two other functions
    * get_requirements(): displays the program requirements
    * calculate_payroll(): calculates an individual one-week paycheck

#### README.md file should include the following items:

* Assignement requirements
* Screenshots of a2_payroll application running (with & without overtime)
* Screenshots of a2_payroll running in Jupyter notebook and include .ipynb file [A2 Payroll .ipynb file](code/payroll.ipynb "Payroll")
* Screenshots of a2 skill sets

#### Assignment Screenshots:

*Screenshot of a2_payroll (without overtime) application running*:

![A2 payroll without overtime](img/payroll_no_overtime.png)

*Screenshot of a2_payroll (with overtime) application running*:

![A2 payroll with overtime](img/payroll_with_overtime.png)

*Screenshot of a2_payroll Jupyter Notebook*:

![A2 payroll Jupyter Notebook](img/payroll_jupyter_notebook.png)

*Screenshot of a2 skillsets*:

ss1                              |ss2                              |ss3
:-----------------------:|:-----------------------:|:-----------------------:
![](img/square_feet_to_acres.png)|![](img/miles_per_gallon.png)|![](img/it_ict_student_percentage.png)