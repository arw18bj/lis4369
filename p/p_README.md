# LIS4369 Extensible Enterprise Solutions

## Avery Withers

### Project Requirements:

- Run demo.py
- If errors, more than likely missing installations
- Test Python Package Installer: pip freeze
- Research how to do the following installations:
    - pandas (only if missing)
    - pandas-datareader (only if missing)
    - matplotlib (only if missing)
- Create at least three fumctions that are called by the program:
    - main(): calls at least two other functions.
    - get_requirements(): displays the program requirements.
    - data_analysis_1(): displays the following data.

#### README.md file should include the following items:

* Assingment requirements
* Screenshot of p_demo running in Visual Studio Code
* Screenshot of p_demo running in Jupyter Notebook and include .ipynb file [Project Demo .ipynb file](demo.ipynb "Demo")
* Screenshots of p skillsets (7-9)

#### Assignment Screenshots:

*Screenshot of Demo running VSC*:

![Demo VSC](img/demo_vsc.png)

*Screenshot of Demo running JNB*:

![Demo JNB](img/demo_jnb.png)

*Screenshot of Demo Graph*:

![Demo Graph](img/demo_graph.png)

#### Skillset Screenshots:

*Screenshot of Skillset 7: Using Lists*:

![Using Lists](img/7_using_lists.png)

*Screenshot of Skillset 8: Using Tuples*:

![Using Tuples](img/8_using_tuples.png)

*Screenshot of Skillset 9: Using Sets*:

![Using Sets](img/9_using_sets.png)