import datetime
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from matplotlib import style

start = datetime.datetime(2010, 1, 1)
end = datetime.datetime(2018, 10, 15)

df = pdr.DataReader("XOM", "yahoo", start, end)

print("\nDeveloper: Avery Withers")
print("\nPrint number of records: ")
print(len(df.index))

print(df.columns)

print("\nPrint data frame: ")
print(df)

print("\nPrint first five lines:")
print(df.head(5))

print("\nPrint last five lines:")
print(df.tail(5))

print("\nPrint first 2 lines:")
print(df.head(2))

print("\nPrint last 2 lines:")
print(df.tail(2))

style.use('ggplot')

df['High'].plot()
df['Adj Close'].plot()
plt.legend()
plt.show()