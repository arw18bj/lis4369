head(mtcars)
head(mtcars,10)

tail(mtcars)

str(mtcars)

colnames(mtcars)

rownames(mtcars)

install.packages("psych")
library(psych)
describe(mtcars)

cor(mtcars)

names(mtcars)

mtcars$hp

plot(mtcars$cyl,mtcars$mpg)

plot(mtcars$disp, mtcars$mpg,
     xlab="Engine Displacement",
     ylab="mpg",
     main="MPG vs Engine Displacement",
     las=1)

barplot(mtcars$cyl,mtcars$hp, main="HP vs Num of Cyls")

qplot(mtcars$cyl,mtcars$hp, main="HP vs Num of Cyls")