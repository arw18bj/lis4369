# LIS4369 Extensible Enterprise Solutions

## Avery Withers

### Assignemnt 5 Requirements:

1. Do skillsets 13-15
2. Do chapter 11 and 12 questions
3. Complete the learn_to_use_r tutorial
4. Code and run the demo.py parts one and two
5. Include the link to the demo file
    * [A5 Demo Link](demo.R "Demo")

#### README.md file should include the following items:

* Assignement requirements, as per A1.
* Screenshots of output from code below, *and* from tutorial.

#### Assignment Screenshots:

*Screenshot of learn_to_use_r tutorial 4 panel*:

![learn_to_use_r tutorial](img/learn_to_use_r.png)

*Learn to use r first graph*:

![first graph](img/learn1.png)

*Learn to use r second graph*:

![second graph](img/learn2.png)

*A5 Demo 4 panel*:

![A5 demo 4 panel](img/demo.png)

*Titanic first graph*:

![titanic first graph](img/titanic1.png)

*Titanic second graph*:

![titanic second graph](img/titanic2.png)