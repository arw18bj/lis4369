# Extensible Enterprise Solutions

## Avery Withers

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development installations
3. Questions
4. Bitbucket repo links:

    a) https://bitbucket.org/arw18bj/lis4369/src/ece084305cfcd201d2c179c3a6e5e1d34a7afb54/a1/a1_README.md

    b) https://bitbucket.org/arw18bj/bitbucketstationlocations/src/master/

#### README.md file should include the following items:

* Screenshot of a1_tip_calculator application running
* Link to A1 .jpynb file: [tip_calculator.ipynb](a1_tip_calculator/tip_calculator.ipynb "A1 Jupyter Notebook")
* git commands w/short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git fetch - Download objects and refs from another repository

#### Assignment Screenshots:

*Screenshot of a1_tip_calculator application running (IDLE)*:

![IDLE Tip Calculator Screenshot](img/a1_idle_screenshot.png)

*Screenshot of a1_tip_calculator application running (vsc)*:

![VSC Tip Calculator Screenshot](img/a1_vsc_screenshot.png)

*Screenshot of a1_tip_calculator application running (JN)*:

![JN Tip Calculator Screenshot](img/a1_jupyter_notebook_screenshot.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/arw18bj/bitbucketstationlocations/ "Bitbucket Station Locations")